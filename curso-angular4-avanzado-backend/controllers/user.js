'use strict'

//cargar modulos
var bcrypt = require('bcrypt-nodejs');
var fs = require('fs');
var path = require('path');

//cargar modelos
var User = require('../models/user');


//servicios
var jwt = require('../services/jwt');

//acciones
function pruebas(request, response) {
    response.status(200).send({
        message: 'probando el controlador de usuarios y la acción',
        user: request.user
    });
}

function saveUser(request, response) {
    //crear el objeto del usuario
    var user = new User();

    //recoger parametros peticion
    var params = request.body;


    if (params.password && params.name && params.surname && params.email) {
        //asignar valores al objeto de usuario
        user.name = params.name;
        user.surname = params.surname;
        user.email = params.email;
        user.role = 'ROLE_USER';
        user.image = null;

        User.findOne({ email: user.email.toLowerCase() }, (error, issetUser) => {

            if (error) {
                response.status(500).send({ message: 'Error al comprobar el usuario' });

            } else if (!issetUser) {
                saveUserDB(params, user, response);
            }
            else {
                response.status(200).send({ message: 'El usuario no pudo registrarse' });
            }
        });

    } else {
        response.status(200).send({ message: 'Introduce los datos correctamente para poder registrar al usuario' });
    }
}

function saveUserDB(params, user, response) {
    bcrypt.hash(params.password, null, null, function (error, hash) {
        user.password = hash;
        console.log(user.password);
        user.save((error, userStored) => {
            if (error) {
                response.status(500).send({ message: 'Error al guardar el usuario' });
            }
            else if (!userStored) {
                response.status(404).send({ message: 'No se ha guardado el usuario' });
            }
            else {
                response.status(200).send({ user: userStored });
            }
        });
    });
}

function login(request, response) {

    var params = request.body;
    var email = params.email;
    var password = params.password;

    User.findOne({ email: email.toLowerCase() }, (error, issetUser) => {

        if (error) {
            response.status(500).send({ message: 'Error al comprobar el usuario' });

        } else if (issetUser) {

            bcrypt.compare(password, issetUser.password, (error, check) => {
                if (check) {

                    //comprobar y generar el token
                    if (params.gettoken) {
                        //devolver el token
                        response.status(200).send({ token: jwt.createToken(issetUser) });
                    } else {
                        response.status(200).send({ user: issetUser });
                    }

                } else {
                    response.status(404).send({ message: 'El usuario no ha podido loguearse correctamente' });
                }
            });
        }
        else {
            response.status(404).send({ message: 'El usuario no ha podido loguearse' });
        }
    });
}

function updateUser(request, response) {

    var userId = request.params.id;
    var update = request.body;

    if (userId != request.user.sub) {
        response.status(500).send({ message: 'No tienes permiso para actualizar el usuario' });
    }

    User.findByIdAndUpdate(userId, update, { new: true }, (error, userUpdate) => {

        if (error) {
            response.status(500).send({ message: 'Error al actualizar el usuario' });

        } else if (!userUpdate) {
            response.status(404).send({ message: 'No se ha podido actualizar el usuario' });

        } else {
            response.status(200).send({ user: userUpdate });
        }
    });
}

function uploadImage(request, response) {
    var userId = request.params.id;
    var file_name = 'No subido...';

    if (request.files) {
        var file_path = request.files.image.path;
        var file_split = file_path.split('\\');
        var file_name = file_split[2];

        var ext_split = file_name.split('\.');
        var file_ext = ext_split[1];

        if (file_ext == 'png' || file_ext == 'jpg' || file_ext == 'jpeg' || file_ext == 'gif') {

            if (userId != request.user.sub) {
                response.status(500).send({ message: 'No tienes permiso para actualizar el usuario' });
            }

            User.findByIdAndUpdate(userId, { image: file_name }, { new: true }, (error, userUpdate) => {

                if (error) {
                    response.status(500).send({ message: 'Error al actualizar el usuario' });

                } else if (!userUpdate) {
                    response.status(404).send({ message: 'No se ha podido actualizar el usuario' });

                } else {
                    response.status(200).send({ user: userUpdate, image: file_name });
                }
            });

        } else {
            fs.unlink(file_path, (error) => {
                if (error) {
                    response.status(200).send({ message: 'extensión no valida y fichero no borrado' });

                } else {
                    response.status(200).send({ message: 'extensión no valida' });
                }
            });
        }
    }
    else {
        response.status(200).send({ message: 'No se han subido ficheros' });
    }
}

function getImageFile(request, response) {

    var imageFile = request.params.imageFile;
    var pathFile = './uploads/users/' + imageFile;
    fs.exists(pathFile, function (exists) {
        if (exists) {
            response.sendFile(path.resolve(pathFile));
        } else {
            response.status(404).send({ message: 'La imagen no existe' });
        }
    });
}


function getKeepers(request, response) {

    User.find({ role: 'ROLE_ADMIN' }).exec((error, users) => {

        if (error) {
            response.status(500).send({ message: 'Error en la petición' });

        } else if (!users) {
            response.status(404).send({ message: 'No hay cuidadores' });

        } else {
            response.status(200).send({ users});
        }
    });
}
module.exports = {
    pruebas,
    saveUser,
    login,
    updateUser,
    uploadImage,
    getImageFile,
    getKeepers
};