'use strict'

//cargar modulos
var fs = require('fs');
var path = require('path');

//cargar modelos
var Animal = require('../models/animal');


//servicios

//acciones
function pruebas(request, response) {
    response.status(200).send({
        message: 'probando el controlador de Animales',
        user: request.user
    });
}

function getAnimals(request, response) {

    Animal.find({}).populate({ path: 'user' }).exec((error, animals) => {
        if (error) {
            response.status(500).send({
                message: 'Error en la petición'
            });
        } else if (!animals) {
            response.status(400).send({
                message: 'No hay animales'
            });
        }
        else {
            response.status(200).send({
                animals
            });
        }
    });
}

function getAnimal(request, response) {

    var animalId = request.params.id;

    Animal.findById(animalId).populate({ path: 'user' }).exec((error, animal) => {
        if (error) {
            response.status(500).send({
                message: 'Error en la petición'
            });
        } else if (!animal) {
            response.status(400).send({
                message: 'No hay animales'
            });
        }
        else {
            response.status(200).send({
                animal
            });
        }
    });
}

function updateAnimal(request, response) {

    var animalId = request.params.id;
    var update = request.body;

    Animal.findByIdAndUpdate(animalId, update, { new: true }, ((error, animal) => {

        if (error) {
            response.status(500).send({
                message: 'Error en la petición'
            });
        } else if (!animal) {
            response.status(400).send({
                message: 'No hay animales'
            });
        }
        else {
            response.status(200).send({
                animal
            });
        }
    }));
}

function saveAnimal(request, response) {
    //crear el objeto del usuario
    var animal = new Animal();

    //recoger parametros peticion
    var params = request.body;

    if (params.name && params.description && params.year) {
        //asignar valores al objeto
        animal.name = params.name;
        animal.description = params.description;
        animal.year = params.year;
        animal.image = null;
        animal.user = request.user.sub;

        Animal.findOne({ name: animal.name }, (error, issetObject) => {

            if (error) {
                response.status(500).send({ message: 'Error al comprobar el animal' });

            } else if (!issetObject) {
                saveAnimalDB(animal, response);
            }
            else {
                response.status(200).send({ message: 'El animal no pudo registrarse' });
            }
        });

    } else {
        response.status(200).send({ message: 'Introduce los datos correctamente para poder registrar al animal' });
    }
}

function saveAnimalDB(animal, response) {

    animal.save((error, objectStored) => {
        if (error) {
            response.status(500).send({ message: 'Error al guardar el animal' });
        }
        else if (!objectStored) {
            response.status(404).send({ message: 'No se ha guardado el animal' });
        }
        else {
            response.status(200).send({ animal: objectStored });
        }
    });
}


function uploadImage(request, response) {
    var animalId = request.params.id;
    var file_name = 'No subido...';

    if (request.files) {
        var file_path = request.files.image.path;
        var file_split = file_path.split('\\');
        var file_name = file_split[2];

        var ext_split = file_name.split('\.');
        var file_ext = ext_split[1];

        if (file_ext == 'png' || file_ext == 'jpg' || file_ext == 'jpeg' || file_ext == 'gif') {

            Animal.findByIdAndUpdate(animalId, { image: file_name }, { new: true }, (error, animalUpdate) => {

                if (error) {
                    response.status(500).send({ message: 'Error al actualizar el animal' });

                } else if (!animalUpdate) {
                    response.status(404).send({ message: 'No se ha podido actualizar el animal' });

                } else {
                    response.status(200).send({ animal: animalUpdate, image: file_name });
                }
            });

        } else {
            fs.unlink(file_path, (error) => {
                if (error) {
                    response.status(200).send({ message: 'extensión no valida y fichero no borrado' });

                } else {
                    response.status(200).send({ message: 'extensión no valida' });
                }
            });
        }
    }
    else {
        response.status(200).send({ message: 'No se han subido ficheros' });
    }
}

function getImageFile(request, response) {

    var imageFile = request.params.imageFile;
    var pathFile = './uploads/animals/' + imageFile;
    fs.exists(pathFile, function (exists) {
        if (exists) {
            response.sendFile(path.resolve(pathFile));
        } else {
            response.status(404).send({ message: 'La imagen no existe' });
        }
    });
}



function deleteAnimal(request, response) {
    var animalId = request.params.id;

    Animal.findByIdAndRemove(animalId, (error, animalRemove) => {

        if (error) {
            response.status(500).send({ message: 'Error en la petición' });
        } else if (!animalRemove) {
            response.status(400).send({ message: 'No se ha borrado el animal' });
        } else {
            response.status(200).send({ animal: animalRemove });
        }
    });
}

module.exports = {
    pruebas,
    saveAnimal,
    getAnimals,
    getAnimal,
    updateAnimal,
    uploadImage,
    getImageFile,
    deleteAnimal
}