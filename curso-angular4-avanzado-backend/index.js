'use strict'

var mongoose = require('mongoose');
var app = require('./app');
var port = process.env.PORT || 3789;
//var Uri='mongodb://localhost:27017/zoo';
var Uri = 'mongodb+srv://dbUser:' + 'Temporal01' + '@cluster0-5lxmg.mongodb.net/zoo?retryWrites=true';

mongoose.Promise = global.Promise;
mongoose.connect(Uri, { useNewUrlParser: true }).then(
    () => {
        console.log('La conexión a la base de datos zoo se ha realizado correctamente...');
        //crear y lanzar server
        app.listen(port, () => {
            console.log('El servidor local con Node y Express está corriendo correctamente...');
        });
    }).catch(
        error => console.log(error)
    );


