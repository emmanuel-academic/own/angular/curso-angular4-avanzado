//importar modulos necesrios para crear modulos
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
//importar componentes
import { GuardarEmailComponent } from './components/guardar-email/guardar-email.component';
import { MostrarEmailComponent } from './components/mostrar-email/mostrar-email.component';
import { MainEmailComponent } from './components/main-email/main-email.component';

// decorar ngModule para cargar los componentes y la configuracion del modulo
@NgModule({
    declarations: [
        GuardarEmailComponent,
        MostrarEmailComponent,
        MainEmailComponent
    ],
    imports: [CommonModule,
    FormsModule],
    exports:[MainEmailComponent]
  })
  export class ModuloEmailModule { }
  