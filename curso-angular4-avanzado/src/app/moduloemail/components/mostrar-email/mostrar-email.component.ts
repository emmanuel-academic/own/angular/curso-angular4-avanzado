import { Component, DoCheck, OnInit } from '@angular/core';

@Component({
  selector: 'mostrar-email',
  templateUrl: './mostrar-email.component.html'
})
export class MostrarEmailComponent implements DoCheck, OnInit {

  title = 'mostrar-email';
  public emailContacto: string;

  ngOnInit(): void {
    this.emailContacto = localStorage.getItem('emailContacto');

  }
  ngDoCheck(): void {
    this.emailContacto = localStorage.getItem('emailContacto');
  }

  borrarEmail(): void {
    localStorage.removeItem('emailContacto');
    localStorage.clear();
    this.emailContacto = null;
    console.log("ASASS");
  }
}
