import { Component, DoCheck, OnInit } from '@angular/core';

@Component({
  selector: 'guardar-email',
  templateUrl: './guardar-email.component.html'
})
export class GuardarEmailComponent  {

  title = 'guardar-email';
  public emailContacto: string;

  guardarEmail(): void {
    localStorage.setItem('emailContacto',this.emailContacto);
  }
}
