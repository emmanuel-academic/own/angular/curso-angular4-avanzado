import { Component, OnInit } from '@angular/core';
import {fadeIn} from '../animation';
@Component({
  selector: 'contact',
  templateUrl: './contact.component.html',
  animations:[fadeIn]
})
export class ContactComponent implements OnInit {

  public title: string = 'contact';
  public emailContacto: string;

  ngOnInit(): void {

    console.log("contact.component cargado")
  }

  guardarEmail(): void {
    localStorage.setItem('emailContacto',this.emailContacto);
  }
}
