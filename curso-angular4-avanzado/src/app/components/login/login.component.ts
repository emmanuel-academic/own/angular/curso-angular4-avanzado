import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { fadeIn } from '../animation';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    animations: [fadeIn],
    providers: [UserService]
})

export class LoginComponent implements OnInit {

    public title: string;
    public user: User;
    public identity;
    public token;
    public status: string;

    constructor(private _route: ActivatedRoute,
        private _router: Router,
        private _userService: UserService) {
        this.title = 'Login';
        this.user = new User('', '', '', '', '', 'ROLE_USER', '');
    }

    public ngOnInit(): void {
        console.log(this._userService.getIdentity());
        console.log(this._userService.getToken());
    }

    public onSubmit(registerForm): void {
        //loguear al usuario y coneguir el objeto
        this.signUp(registerForm);
    }

    private signUp(registerForm: any) {
        this._userService.signup(this.user).subscribe(response => {
            this.identity = response.user;
            if (!this.identity || !this.identity._id) {
                alert('El usuario no se ha logueado correctamente');
            }
            else {
                this.identity.password = '';
                localStorage.setItem('identity', JSON.stringify(this.identity));
                //conseguir el token
                this.getLoginToken(registerForm);
            }
        }, error => {
            var errorMessage = (<any>error);
            if (errorMessage != null) {
                var body = JSON.parse(error._body);
                this.status = 'error';
            }
        });
    }

    private getLoginToken(registerForm: any): void {
        this._userService.signup(this.user, 'true').subscribe(response => {
            this.token = response.token;
            if (this.token.length <= 0) {
                alert('El token no se ha generado');
            }
            else {
                localStorage.setItem('token', JSON.stringify(this.token));
                this.status = 'success';
                this._router.navigate(['/home'])
            }
        }, error => {
            console.log(<any>error);
        });
    }
}