import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { fadeIn } from '../animation';
import { User } from '../../models/user';
import { GLOBAL } from '../../services/global';
import { UserService } from '../../services/user.service';

@Component({

    selector: 'register',
    templateUrl: './register.component.html',
    animations: [fadeIn],
    providers: [UserService]
})

export class RegisterComponent implements OnInit {

    public title: string;
    public user: User;
    public status: string;

    constructor(private _route: ActivatedRoute,
        private _router: Router,
        private _userService: UserService) {

        this.title = 'Identificate';
        this.user = new User('', '', '', '', '', 'ROLE_USER', '');
    }

    ngOnInit(): void {
        console.log('register.component cargado');
    }
    onSubmit(registerForm): void {
        this._userService.register(this.user).subscribe(
            response => {
                if (response.user && response.user._id) {
                    this.status = 'success';
                    this.user = new User('', '', '', '', '', 'ROLE_USER', '');
                    registerForm.reset();
                } else {
                    this.status = 'error';
                }
            },
            error => {
                console.log(<any>error);
            }
        );
    }
}